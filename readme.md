# Magento 'rwd' child theme for Rhino Racks

![Rhino Racks Logo](http://www.rhinorack.com/app_themes/rr/images/rr-logo.png)


## Installation
1. Go to [https://bitbucket.org/mrkdevelopment/rhino-racks-magento-child-theme/downloads](https://bitbucket.org/mrkdevelopment/rhino-racks-magento-child-theme/downloads) and click on `Download repository` link on the `Downloads` tab. A ZIP file will be downloaded.
2. Uncompress the file to a temporary location. A folder prefixed with `mrkdevelopment-rhino-racks-magento-child-theme` will be created. 
3. Copy the `app` folder within this folder to your main Magento folder (Merge the Magento and Theme's folder).
4. Copy the `skin` folder within this folder to your main Magento folder (Merge the Magento and Theme's folder).
5. Confirm that you can see `app/design/frontend/rhinoracks` folder within Magento. This is the `rwd` child theme.
6. Login into the admin and goto `System > Configuration`. Click on `Design` on the left sidebar (under General) 
7 Click on Package to deploy and enter `rhinoracks` as the `Current Package Name` and click `Save Config`.
	![Child theme setting](app/design/frontend/rhinoracks/default/screenshots/rhino-racks-magento-child-theme-setting.png)
8. Empty the `var/cache` folder or go to `Sytem > Cache Management` and click on `Flush Cache Storage`.
9. Visit a rack or accessory products details page and find the additional data being rendered
	![Product details page](app/design/frontend/rhinoracks/default/screenshots/products-details-page.png)
	
## Adding 'Rack Selector Widget' to a page
The [RhinoRacks APIConnector Module](https://bitbucket.org/mrkdevelopment/rhino-magento-api-connecter-1.x) ships a `Rack Selector Widget` - a HTML block which is used to filter racks by selecting options from Rack make, model, year dropdown, shown in the screenshot below.

![Rack Selector](app/design/frontend/rhinoracks/default/screenshots/rack-selector.png)

To add the rack selector widget, login into the admin interface of Magento and goto `CMS > Pages`. Select the page where you would like to place this widget, then click `Content` in the left sidebar. In the content editor, place the cursor where you would like to insert the widget and press `Insert Widget` icon (2nd button on the top right corner of the WySIWyG toolbar), select `RhinoRacks's Rack Selector` widget and press `Insert Widget` button. Save page to insert the widget.

![Insert Widget](app/design/frontend/rhinoracks/default/screenshots/insert-widget.png)