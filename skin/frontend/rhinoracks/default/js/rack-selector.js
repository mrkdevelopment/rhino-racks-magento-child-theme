/**
 * Selected Rack Selector URL
 */
var selected_url = '';


/**
 * Initialize
 */
jQuery(document).ready(function(event){
    // Initial Make Dropdown
    populateMakeDropdown();


    // Make dropdown change listener
    jQuery(document).on('change', 'select#make', function(event){
        var v = jQuery(this).val();
        if (v) {
            var parts = v.split("|");
            selected_url = parts[1];
            jQuery('#model').removeAttr('disabled');
            populateModelsDropdown(parts[0]);

        }
    });

    // Model dropdown change listener
    jQuery(document).on('change', 'select#model', function(event){
        var v = jQuery(this).val();
        if (v) {
            var parts = v.split("|");
            selected_url = parts[1];
            jQuery('#year').removeAttr('disabled');
            populateYearsDropdown(parts[0]);

        }
    });


    // Year dropdown change listener
    jQuery(document).on('change', '#year', function(event){
        var v = jQuery(this).val();
        if (v) {
            var parts = v.split("|");
            selected_url = parts[1];
            jQuery('#body').removeAttr('disabled');
            populateBodyDropdown(parts[0]);
        }
    });

    // Body dropdown change listener
    jQuery(document).on('change', '#body', function(event){
        var v = jQuery(this).val();
        if (v) {
            var parts = v.split("|");
            selected_url = parts[1];
            window.location.href = parts[1];
        }
    });


    // Rack Selector Form Submit Action Handler.
    jQuery('#rack-selector-form').submit (function(e){
        e.preventDefault();
        if(selected_url){
            window.location.href = selected_url;
        } else {
            alert('Please select a make');
        }
    })

});


/**
 * Helper function to fetch make api data and populate dropdown
 */
function populateMakeDropdown(){
    populateDropdown('#make', '/apiconnector/RackCategories/manufacturers', {}, 'Select Make')
        .done(function(data){
            jQuery('#model').empty().removeAttr('disabled', 'disabled').closest('.select-validate-select').addClass('select-disabled');
            jQuery('#year').empty().removeAttr('disabled', 'disabled').closest('.select-validate-select').addClass('select-disabled');
            jQuery('#body').empty().removeAttr('disabled', 'disabled').closest('.select-validate-select').addClass('select-disabled');

    });
}

/**
 * Helper function to fetch model api data and populate dropdown
 */

function populateModelsDropdown(manufacturer_id){
    populateDropdown('select#model', '/apiconnector/RackCategories/models', {"manufacturer_id" : manufacturer_id}, 'Select Model')
        .done(function(data){
            jQuery('#year').empty().removeAttr('disabled', 'disabled').closest('.select-validate-select').addClass('select-disabled');
            jQuery('#body').empty().removeAttr('disabled', 'disabled').closest('.select-validate-select').addClass('select-disabled');
        });
}


/**
 * Helper function to fetch year api data and populate dropdown
 */

function populateYearsDropdown(model_id){
    populateDropdown('select#year', '/apiconnector/RackCategories/years', {"model_id" : model_id}, 'Select Model');
}



/**
 * Populate Body Dropdown.
 *
 * @param  {[type]} year_id [description]
 *
 * @return {[type]}         [description]
 */
function populateBodyDropdown(year_id){
    populateDropdown('select#body', '/apiconnector/RackCategories/bodies', {"year_id" : year_id}, 'Select Body');
}


/**
 * Populate the dropdown using data from
 * API call. Returns a promise object.
 */
function populateDropdown(selector, base_url, params, title){
    var dropdown = jQuery(selector);
    dropdown.empty();

    params.t = (new Date).getTime();

    var defer =  jQuery.get(base_url, params)


    defer.done(function(data, textStatus, xhr){

        var items = [];
        items.push("<option value=''>");
        items.push(title);
        items.push("</option>");

        for (var i = 0; i < data.length; i++) {
            var resource = data[i];
            items.push("<option value='"+ resource.id + "|" + resource.url+ "'>");
            items.push(resource.name);
            items.push("</option>");
        };

        dropdown.append(items.join(""));

    });

    return defer.promise();
}